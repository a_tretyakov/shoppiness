# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from ckeditor.fields import RichTextField

class ArticleManager(models.Manager):

    def published(self):
        return self.filter(draft=False)

    def drafted(self):
        return self.filter(draft=True)



class Article(models.Model):
    title = models.CharField(u'Заголовок', max_length=50)
    slug = models.SlugField(u'ЧПУ')
    image = models.ImageField('Image', upload_to='img/', blank=True)
    tease = RichTextField(u'Тизер', blank=True)
    body = RichTextField(u'Контент')
    draft = models.BooleanField(u'В черновики', default=False)
    created_at = models.DateTimeField(u'Дата создания', default=datetime.now)
    published_at = models.DateTimeField(u'Дата публикации', default=datetime.now)
    #category = models.ForeignKey(Category, related_name='entries', verbose_name = "Категория")

    objects = ArticleManager()

    class Meta:
        verbose_name = u'статью'
        verbose_name_plural = u'статьи'
        ordering = ('-published_at',)

    def __unicode__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('article_detail', (), {'slug': self.slug})
