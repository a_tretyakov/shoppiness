# -*- coding: utf-8 -*-
from django import template
from articles.models import Article

register = template.Library()

@register.inclusion_tag('articles/article_tag.html')
def articles_tag(articles):
    entries = Article.objects.all().order_by('?')[:1]
    return {
        'entries': entries
    }