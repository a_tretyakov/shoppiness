from django.conf.urls import url
from .views import article_list, article_detail


urlpatterns = [
    url(r'^$', article_list, name='article_list'),
    url(r'^(?P<slug>[-\w]+)/$', article_detail, name='article_detail')
]
