
# Create your views here.
from .models import Article

from django.shortcuts import get_object_or_404, render

def article_list(request):
    entries = Article.objects.published()
    return render(request, 'articles/article_list.html', {'entries': entries}, request.FILES)
'''
def category(request, id, slug):
    category = get_object_or_404(Category, id=id,
                                           slug=slug)
    entries = category.entries.published()
    return render_to_response('blog/index.html', {'entries': entries})
'''

def article_detail(request, slug):
    entry = get_object_or_404(Article, draft=False,
                                         slug=slug)
    return render(request, 'articles/article_detail.html', {'entry': entry})