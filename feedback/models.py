#! coding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.forms import ModelForm, Textarea, TextInput
#
#
class Contact(models.Model):
    mail = models.CharField('E-mail:', max_length=255)
    name = models.CharField('Имя:', max_length=255)
    phone = models.CharField('Номер телефона:', max_length=255)
    text = models.TextField('Сообщение:')
#
    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
#
    def __unicode__(self):
        return '%s' % self.name
#
#
class ContactForm(ModelForm):
#
    class Meta:
        model = Contact
        exclude = []
        widgets = {
            'text': Textarea(attrs={
		'cols': 80, 'rows': 10, 'id': 'widgetu2207_input', 'class': 'wrapped-input', 'name': 'custom_U2207','placeholder' : 'Текст', 'tabindex': '4', 'placeholder': 'Текст сообщения',}),
            'name': TextInput(attrs={
        'id': 'widgetu2184_input', 'placeholder': 'Имя', 'class': 'wrapped-input', 'type': 'text', 'name': 'custom_U2184', 'tabindex': '1', 'spellcheck':'false'}),
            'mail': TextInput(attrs={
        'id': 'widgetu2190_input','placeholder': 'E-mail', 'class': 'wrapped-input', 'type': 'text', 'name': 'Email', 'tabindex': '2', 'spellcheck':'false'}),
            'phone': TextInput(attrs={
        'id': 'phone', 'placeholder' : 'Номер телефона'})
        }
