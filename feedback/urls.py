# -*- coding: utf-8 -*-
from django.conf.urls import url

from feedback.views import ContactCreateView

urlpatterns = [
    url(r'^$', ContactCreateView.as_view(), name='contacts'),
]
