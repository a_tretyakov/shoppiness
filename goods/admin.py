from django.contrib import admin
from goods.models import GoodsEntry, Order

class GoodsEntryAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        'slug': ('title',),
    }

    list_display = ('created_at', 'published_at', 'title', 'draft')
    list_display_links = ('title',)
    list_filter = ('created_at', 'published_at', 'draft')
    date_hierarchy = 'created_at'
    search_fields = ('title', 'preview', 'detail')

    exclude = ('created_at',)
    fieldsets = (
        (None, {'fields': ('title', 'slug', 'price', 'preview', 'detail', 'image', 'add_image1', 'add_image2')}),
        ('Properties', {'fields': ('draft', 'published_at')}),
    )


'''
admin.site.register(Category, CategoryAdmin)
'''
admin.site.register(GoodsEntry, GoodsEntryAdmin)



admin.site.register([Order])