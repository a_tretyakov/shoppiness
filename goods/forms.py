# -*- coding: utf-8 -*-
from django.forms import ModelForm, TextInput, HiddenInput


from .models import Order
class OrderForm(ModelForm):

    class Meta:
        model = Order
        exclude = []
        widgets = {
            'phone': TextInput(attrs={
                'id': 'pass', 'class': 'form-control input-lg', 'placeholder': u'Телефон'}),
            'first_name': TextInput(attrs={
                'id': 'pass', 'class': 'form-control input-lg', 'placeholder': u'Имя'}),
            'address': TextInput(attrs={
                'id': 'pass', 'class': 'form-control input-lg', 'placeholder': u'Адрес'}),
            'good': HiddenInput(attrs={
                'id': 'pass', 'class': 'form-control input-lg', 'placeholder': u'Товар'},),
        }