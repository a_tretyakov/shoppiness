# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ckeditor.fields import RichTextField
from datetime import datetime
from django.db import models


# Create your models here.
class GoodsManager(models.Manager):

    def published(self):
        return self.filter(draft=False)

    def drafted(self):
        return self.filter(draft=True)


class GoodsEntry(models.Model):

    title = models.CharField(u'Название', max_length=50)
    slug = models.SlugField(u'ЧПУ')
    price = models.DecimalField(u'Цена', max_digits=10, decimal_places=2)
    image = models.ImageField(u'Изображение', upload_to='img/', blank=True)
    add_image1 = models.ImageField(u'Дополнительное изображение 1', upload_to='img/', blank=True)
    add_image2 = models.ImageField(u'Дополнительное изображение 2', upload_to='img/', blank=True)
    preview = RichTextField(u'Краткое описание', blank=True)
    detail = RichTextField(u'Детальное описание')
    draft = models.BooleanField(u'Скрыть товар в каталоге', default=False)
    created_at = models.DateTimeField(u'Дата создания', default=datetime.now)
    published_at = models.DateTimeField(u'Дата публикации', default=datetime.now)

    objects = GoodsManager()

    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'
        ordering = ('-published_at',)

    def __unicode__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        return ('goods_details', (), {'slug': self.slug})




class Order(models.Model):
    phone = models.CharField(u'Номер телефона:', max_length=255)
    first_name = models.CharField(u'Имя:', max_length=255)
    address = models.CharField(u'Адрес:', max_length=255)
    good = models.ForeignKey('GoodsEntry', blank=True)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

    def __unicode__(self):
        return '%s' % self.phone
