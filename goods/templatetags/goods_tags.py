# -*- coding: utf-8 -*-
from django import template
from goods.models import GoodsEntry

register = template.Library()

@register.inclusion_tag('goods/goods_tag.html')
def goods_tag(goods):
    goods = GoodsEntry.objects.all().order_by('-created_at')[:8]
    return {
        'goods': goods
    }

@register.inclusion_tag('goods/goods_tag_bottom.html')
def goods_tag_bottom(goods):
    goods = GoodsEntry.objects.reverse()[:4]
    return {
        'goods': goods
    }

@register.inclusion_tag('goods/goods_tag.html')
def goods_tag_inner(goods):
    goods = GoodsEntry.objects.all().order_by('?')[:4]
    return {
        'goods': goods
    }



