from django.conf.urls import url
from .views import goods_details, goods_list, OrderCreateView

urlpatterns = [
    url(r'^$', goods_list, name='goods_list'),
    url(r'^(?P<slug>[-\w]+)/$', goods_details, name='goods_details'),
    url(r'^(?P<slug>[-\w]+)/iframe.html', OrderCreateView.as_view(), name='order'),

]
