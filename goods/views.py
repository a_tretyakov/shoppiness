# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render
from goods.models import GoodsEntry, Order
from django.conf import settings
from django.views.generic import CreateView, DetailView
from django.core.mail import send_mail
from .forms import OrderForm

def goods_list(request):
    goods = GoodsEntry.objects.all()
    context = {
        'goods': goods
    }
    return render(request, 'goods/goods_list.html', context, request.FILES)

def goods_details(request, slug):
    good = get_object_or_404(GoodsEntry, draft=False,
                                         slug=slug)
    return render(request, 'goods/goods_detail.html', {'good': good})


class OrderCreateView(CreateView, DetailView):
    form_class = OrderForm
    model = GoodsEntry
    template_name = 'goods/iframe.html'
    success_url = '/thanks/'
#
    def form_valid(self, form):
        good = GoodsEntry.objects.get(slug=self.kwargs['slug'])
        form.instance.good = good
        message = ' Адрес покупателя: {address},\nИмя: {first_name},\nТел.: {phone}, \nТовар на заказ: '.format(
            address=form.cleaned_data.get('address').encode('utf-8'),
            first_name=form.cleaned_data.get('first_name').encode('utf-8'),
            phone=form.cleaned_data.get('phone').encode('utf-8'),
        )
        message += good.title.encode('utf-8')
        send_mail(
            subject='Новый заказ',
            message=message,
            from_email='contact-form@myapp.com',
            recipient_list=settings.LIST_OF_EMAIL_RECIPIENTS
        )
        return super(OrderCreateView, self).form_valid(form)