from django.contrib import admin

from .models import Banner

admin.site.register(Banner)


from django.contrib import admin
from .models import FrequentlyQuestion


class FrequentlyQuestionAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        'slug': ('question',),
    }

    list_display = ('created_at', 'published_at', 'question', 'draft')
    list_display_links = ('question',)
    list_filter = ('created_at', 'published_at', 'draft')
    date_hierarchy = 'created_at'
    search_fields = ('question', 'answer')

    exclude = ('created_at',)
    fieldsets = (
        (None, {'fields': ('question', 'slug', 'answer')}),
        ('Properties', {'fields': ('draft', 'published_at')}),
    )


admin.site.register(FrequentlyQuestion, FrequentlyQuestionAdmin)