# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from ckeditor.fields import RichTextField


class Banner(models.Model):
    title = models.CharField(u'Название', max_length=255)
    subtitle = models.CharField(u'Подзаголовок', max_length=255)
    image = models.ImageField(u'Изображение', upload_to='img/', blank=True)

    class Meta:
        verbose_name = u'Баннер'
        verbose_name_plural = u'Баннер'

    def __unicode__(self):
        return '%s' % self.title





class FrequentlyQuestionManager(models.Manager):

    def published(self):
        return self.filter(draft=False)

    def drafted(self):
        return self.filter(draft=True)



class FrequentlyQuestion(models.Model):
    question = models.CharField(u'Текст вопроса', max_length=255)
    slug = models.SlugField(u'ЧПУ')
    answer = RichTextField(u'Ответ')
    draft = models.BooleanField(u'В черновики', default=False)
    created_at = models.DateTimeField(u'Дата создания', default=datetime.now)
    published_at = models.DateTimeField(u'Дата публикации', default=datetime.now)

    objects = FrequentlyQuestionManager()

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'вопросы'
        ordering = ('-published_at',)



    def __unicode__(self):
        return u'%s' % self.question