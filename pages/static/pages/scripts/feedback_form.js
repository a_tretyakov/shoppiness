/**
 * Created by pathfinder on 12.12.16.
 */
/**
 * Created by pathfinder on 07.12.16.
 */
$('.js-form-2').on('submit', 'form', function (event) {
  event.preventDefault();
  var $this = $(this);
  $.ajax({
    url: $this.attr('action'),
    type: $this.attr('method'),
    data: $this.serialize(),
    success: function (new_html) {
      $this.closest('.js-form-2').html(new_html);
    },
    error: function () {
      alert('Произошла ошибка, попробуйте позже.');
    }
  })
})