from django.shortcuts import render
from .models import Banner, FrequentlyQuestion

def home(request):
    banner = Banner.objects.get()

    context = {
        'banner': banner
    }
    return render(request, 'pages/index.html', context, request.FILES)

def about(request):
    return render(request, 'pages/about.html')

def faq(request):
    entries = FrequentlyQuestion.objects.published()
    return render(request, 'pages/faq.html', {'entries': entries}, request.FILES)