from django.contrib import admin
from .models import Review


class ReviewAdmin(admin.ModelAdmin):
    prepopulated_fields = {
        'slug': ('reviewer',),
    }

    list_display = ('created_at', 'published_at', 'reviewer', 'draft')
    list_display_links = ('reviewer',)
    list_filter = ('created_at', 'published_at', 'draft')
    date_hierarchy = 'created_at'
    search_fields = ('reviewer', 'text')

    exclude = ('created_at',)
    fieldsets = (
        (None, {'fields': ('reviewer', 'slug', 'text', 'image')}),
        ('Properties', {'fields': ('draft', 'published_at')}),
    )


admin.site.register(Review, ReviewAdmin)
