# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-12 13:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='review',
            options={'ordering': ('-published_at',), 'verbose_name': '\u043e\u0442\u0437\u044b\u0432', 'verbose_name_plural': '\u043e\u0442\u0437\u044b\u0432\u044b'},
        ),
    ]
