# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from ckeditor.fields import RichTextField

class ReviewManager(models.Manager):

    def published(self):
        return self.filter(draft=False)

    def drafted(self):
        return self.filter(draft=True)



class Review(models.Model):
    reviewer = models.CharField(u'Автор отзыва', max_length=50)
    slug = models.SlugField(u'ЧПУ')
    image = models.ImageField('Image', upload_to='img/', blank=True)
    text = RichTextField(u'Текст отзыва')
    draft = models.BooleanField(u'В черновики', default=False)
    created_at = models.DateTimeField(u'Дата создания', default=datetime.now)
    published_at = models.DateTimeField(u'Дата публикации', default=datetime.now)

    objects = ReviewManager()

    class Meta:
        verbose_name = u'отзыв'
        verbose_name_plural = u'отзывы'
        ordering = ('-published_at',)



    def __unicode__(self):
        return u'%s' % self.reviewer