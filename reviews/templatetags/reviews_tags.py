# -*- coding: utf-8 -*-
from django import template
from reviews.models import Review

register = template.Library()

@register.inclusion_tag('reviews/reviews_tags.html')
def reviews_tag(reviews):
    reviews = Review.objects.all().order_by('-created_at')[:3]
    return {
        'reviews': reviews
    }