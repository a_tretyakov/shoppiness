from django.conf.urls import url
from .views import review_list


urlpatterns = [
    url(r'^$', review_list, name='review_list'),
]
