from django.shortcuts import render
from .models import Review


def review_list(request):
    reviews = Review.objects.published()
    return render(request, 'reviews/review_list.html', {'reviews': reviews}, request.FILES)