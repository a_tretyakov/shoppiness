from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from django.views.generic.base import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^shop/', include('goods.urls')),
    url(r'^reviews/', include('reviews.urls')),
    url(r'^contacts/', include('feedback.urls')),
    url(r'^articles/', include('articles.urls')),
    url(r'^thanks/$', TemplateView.as_view(template_name='goods/order_succes.html')),
    url(r'^thanks_feedback/$', TemplateView.as_view(template_name='feedback/thanks.html')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^', include('pages.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
